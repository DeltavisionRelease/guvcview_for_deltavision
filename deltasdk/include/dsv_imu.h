/*
 * dsv_imu.h
 *
 *  Created on: 2018年6月3日
 *  Author        : allen
 */

#ifndef DSV_IMU_H_
#define DSV_IMU_H_
#define 	BUFFER_LENGTH	64
#define		DEBUG_ENABLED	1

#define AccSen          0.0078125       //g/lsb @ +/- 16g
#define GyroSen         0.01524         //¡ã/s/lsb @ 500
#define TempSen         0.5                     //K/LSB center temperature is 23
#define MagxySen        0.3                     //uT/lsb
#define MagzSen         0.15            //uT/lsb


#define		M_PI				3.14159265358979323846
#define		HALF_PI				(M_PI / 2)
#define		DEG2RAD				(M_PI / 180.f)
#define		RAD2DEG				(180.f / M_PI)

/* Report Numbers */

#define SET_FAIL				0x00
#define SET_SUCCESS				0x01
#define GET_FAIL				0x00
#define GET_SUCCESS				0x01
#define SUCCESS			 		1
#define FAILURE					-1
#define BUFFER_LENGTH				65
#define TIMEOUT					2000
#define CALIB_TIMEOUT				5000
#define DESCRIPTOR_SIZE_ENDPOINT		29
#define DESCRIPTOR_SIZE_IMU_ENDPOINT		23

#define IMU_INFO_LENTH 20

int InitExtensionUnit(char *busname, char local);
int GetIMUData(int imu_fd, float * Acc_dat, float * Gyro_dat, float *  Mag_dat) ;

#endif /* DSV_IMU_H_ */
